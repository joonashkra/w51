import React, { useState } from 'react'

export default function FormValidation() {

    const ages = [...Array(126).keys()]

    const [age, setAge] = useState("")
    const [name, setName] = useState("")
    const [error, setError] = useState(false)

    let person = {
        name: name,
        age: age
    }

    const handleAgeInput = (e) => {
        setAge(e.target.value)
        person.age = age
    }

    const handleNameInput = (e) => {
        if (name.charAt(0) !== name.charAt(0).toUpperCase()) {
            setError(true);
        } else {
            setError(false);
        }
        setName(e.target.value)
        person.name = name
    }

    return (
        <div>
            <form>
                <input type='text' name='name' onChange={handleNameInput}/>
                <select name='age' onChange={handleAgeInput}>
                    {ages.map((age, i) => (
                        <option key={i} value={age}>{age}</option>
                    ))}
                </select>
            </form>
            {error ? (
                <p>Name must start with a capital letter.</p>
            ) : (
                <p>{JSON.stringify(person)}</p>
            )}
        </div>
    )
}
